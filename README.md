Located in Fort Myers, Buckingham Farms is an 80+ acre ranch, hydroponic farm, country store, counter service eatery and wedding destination. We provide the finest farm-produced products and one-of-a-kind events in an unmatched countryside setting. We’re taking farm-to-table food to the next level.

Address: 12931 Orange River Blvd, Fort Myers, FL 33905, USA

Phone: 239-206-2303

Website: [https://www.buckinghamfarmsonline.com](https://www.buckinghamfarmsonline.com)
